# fleshOrder

web application where the user can scan a Qr Code in his table to obtain the digital menu, with which he can make his order

# fleshapp app

This is the source code for the fleshapp app.

## Requirements
- [Flutter](https://flutter.dev/docs/get-started/install)
- [Android Studio](https://developer.android.com/studio/install)

## Setup
- Clone the project
```shell script
$> git clone https://gitlab.com/The_boring_way/fleshorder.git
```
- Go in the project

- Just open the project with Android Studio in the root folder and let gradle do the work.
- Don't forget to set the flutter SDK path in Preference/Languages & Frameworks/Flutter

```
$> bundle exec fastlane develop env:devController version_code:6 
```

#### Pre-prod and dev environments
The deployments those environments are handled via [Firebase App Distribution](https://firebase.google.com/docs/app-distribution).
